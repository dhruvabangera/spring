package com.rajesh.SpringDemo2;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class MyEventListner implements ApplicationListener {

	public void onApplicationEvent(ApplicationEvent event) {
		System.out.println("Event occured : "+event.toString());
		
	}

}
