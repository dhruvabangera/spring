package com.rajesh.SpringDemo2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.ImportResource;
import javax.annotation.*;

public class Square implements Shape{

	Point squarePoint;
	Point qualifierPoint;
	
	public void draw() {
		System.out.println("Drawing square :"+getSquarePoint().getX()+" "+getSquarePoint().getY()+" "+getQualifierPoint().getX()+" "+getQualifierPoint().getY());
	}

	public Point getSquarePoint() {
		return squarePoint;
	}

	@Autowired
	public void setSquarePoint(Point squarePoint) {
		this.squarePoint = squarePoint;
	}

	public Point getQualifierPoint() {
		return qualifierPoint;
	}
	@Resource(name="squarePoint2")
	public void setQualifierPoint(Point qualifierPoint) {
		this.qualifierPoint = qualifierPoint;
	}
	
	

}
