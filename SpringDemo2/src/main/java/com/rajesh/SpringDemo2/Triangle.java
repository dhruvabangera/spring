package com.rajesh.SpringDemo2;

import java.util.List;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;

import com.rajesh.SpringDemo2.Point;

public class Triangle implements InitializingBean, DisposableBean, Shape {
	String height,width;
	
	Point pointA, pointB,pointC;
	
	List<Point> points;
	
	ApplicationContext context;

	
	public Triangle(String height){
		this.height = height;
	}
	public Triangle(String height, String width){
		this.height = height;
		this.width = width;
	}
	
/*	public Point getNewPoint(){
		return (Point)context.getBean("triangle-alias");
	}
*/
	public void draw(){
		System.out.println("Drawing triangle Height:"+height+" Wight: "+width);
		System.out.println("Point A :"+getPointA().getX()+"  "+getPointA().getY());
		System.out.println("Point B :"+getPointB().getX()+"  "+getPointB().getY());
		System.out.println("Point C :"+getPointC().getX()+"  "+getPointC().getY());
		
		for(Point point : points){
			System.out.println("Point :"+point.getX()+"  "+point.getY());
		}
	}

	public String getHeight() {
		return height;
	}
	public String getWidth() {
		return width;
	}
	public Point getPointA() {
		return pointA;
	}
	public void setPointA(Point pointA) {
		this.pointA = pointA;
	}
	public Point getPointB() {
		return pointB;
	}
	public void setPointB(Point pointB) {
		this.pointB = pointB;
	}
	public Point getPointC() {
		return pointC;
	}
	public void setPointC(Point pointC) {
		this.pointC = pointC;
	}
	public List<Point> getPoints() {
		return points;
	}
	public void setPoints(List<Point> points) {
		this.points = points;
	}
/*	public void setApplicationContext(ApplicationContext context) throws BeansException {
		this.context = context;
		
	}*/
	public void afterPropertiesSet() throws Exception {
		System.out.println("Initilization Bean called .. all property set");
		
	}
	public void destroy() throws Exception {
		System.out.println("Disposable Bean called ...");
		
	}

	public void myInit(){
		System.out.println("Custom init called...");
	}
	
	public void myDestroy(){
		System.out.println("Custom destroy called...");
	}

	/*public void setName(String name) {
		this.name = name;
	}*/
	
	
}
