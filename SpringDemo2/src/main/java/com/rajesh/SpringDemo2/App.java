package com.rajesh.SpringDemo2;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.rajesh.SpringDemo2.Triangle;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        AbstractApplicationContext context = new FileSystemXmlApplicationContext("C:\\Users\\Rajesh\\git\\spring\\SpringDemo2\\SpringConfig.xml");
    	context.registerShutdownHook();
        Triangle t = (Triangle)context.getBean("triangle-alias");
    	t.draw();
    	
    	Shape shape = (Shape)context.getBean("circle");
    	shape.draw();
    	
    	Shape squareshape = (Shape)context.getBean("square");
    	squareshape.draw();
    	
    	
    }
}
