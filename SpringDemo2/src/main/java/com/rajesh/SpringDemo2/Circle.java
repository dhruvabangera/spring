package com.rajesh.SpringDemo2;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Component
public class Circle implements Shape, ApplicationEventPublisherAware{

	Point center;
	@Autowired
	private MessageSource messageSource;
	private ApplicationEventPublisher publisher;
	

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public Point getCenter() {
		return center;
	}

	@Required
	@Resource(name="squarePoint2")
	public void setCenter(Point center) {
		this.center = center;
	}

	public void draw() {
		System.out.println("Drawing within circle. :"+getCenter().getX()+" "+getCenter().getY());
		System.out.println("Greeting from circle:"+ messageSource.getMessage("greeting", null, "Default Greeting", null));
		System.out.println("Points from circle:"+ messageSource.getMessage("circle.points", new Object[]{999, 888}, "Default Greeting", null));
		//publishing the event which will be handled in listner
		DrawEvent drawEvent = new DrawEvent(this);
		publisher.publishEvent(drawEvent);
	}
	@PostConstruct
	public void initializeCircle(){
		System.out.println("Init of circle");
	}
	@PreDestroy
	public void destructionCircle(){
		System.out.println("Destroy of circle");
	}

	public void setApplicationEventPublisher(ApplicationEventPublisher publisher) {
		this.publisher = publisher;
	}

}
