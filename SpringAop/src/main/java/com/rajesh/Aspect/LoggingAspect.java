package com.rajesh.Aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

import com.rajesh.model.Circle;

@Aspect
public class LoggingAspect {
	//methods of the class becomes "advice"
	
	//@Before(value = "execution(public String com.rajesh.model.Circle.getName())")
	//@Before(value = "execution(public String com.rajesh.model.Circle.getName())")
	//@Before(value = "execution(public * get*())")
	// 1 or more args
	//@Before(value = "execution(public * get*(*))")
	// 0 or more arguments
	@Before(value = "execution(public * get*(..))")
	public void LoggingAdvice(){
		System.out.println("Advice run. Get methid is called.");
	}
	
	@After(value="allSetterMethods()")
	public void SetterAdvice(){
		System.out.println("After setter method  called");
	}
	
	//called after successfull method execution only
	@AfterReturning(value="allSetterAfterSuccessfullReturn()", returning="returnString")
	public void SetterAdviceOnSuccess(){
		System.out.println("After setter method  called and returned success");
	}
	
	@AfterThrowing(value="allSetterAfterExecution()")
	public void SetterAdviceOnError(){
		System.out.println("After setter method  called and returned on error");
	}
	
	
	
	//@Before("allGetters()")
	//you can concatenate 2  pointcuts
	//@Before("allGetters() && allCircleMethods()  ")
	//we can primt msga as per method
	@Before("allCircleMethods()")
	public void secondAdvice(JoinPoint jointPoint){
		Circle circle = (Circle)jointPoint.getTarget();
		circle.setName("MyCircle");
		System.out.println("Second advice executed."+jointPoint.toString()+" "+jointPoint.getTarget()+" "+circle.getName());
	}
	
	//all methods which match with the arguments and apply for that
	@Before("args(name)")
	public void stringArgumentMethods(String name){
		System.out.println("A method taking string argument has been called. The values is :"+name);
	}
	
	//@Around("allGetters()")
	@Around("@annotation(com.rajesh.Aspect.Loggable)")
	public Object myAroundAdvice(ProceedingJoinPoint proceedingJointPoint){
		Object returnValue=null;
		//code before target execution
		//actually executed the method.
		System.out.println("Before around advice...");
		//we can decide if we want to call target method or not
		try {
			returnValue = proceedingJointPoint.proceed();
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println("Exception around advice...");
		}
		//code after target execution
		System.out.println("After around advice...");
		return returnValue;
	}
	
	//we can refer this is aspect
	@Pointcut("execution(* get*())")
	public void allGetters() {}
	
	//This pointcuts will applyt ot all methods of circle class
	//@Pointcut(value="within(com.rajesh.model.*)")
	//@Pointcut(value="within(com.rajesh.model..)")
	@Pointcut(value="within(com.rajesh.model.Circle)")
	public void allCircleMethods() {}
	
	@Pointcut(value="execution(public * set*(..))")
	public void allSetterMethods(){}
	
	@Pointcut(value="execution(public * set*(..))")
	public void allSetterAfterSuccessfullReturn(){}
	
	@Pointcut(value="execution(public * set*(..))")
	public void allSetterAfterExecution(){}
	

	

}
