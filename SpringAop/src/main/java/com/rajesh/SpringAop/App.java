package com.rajesh.SpringAop;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.rajesh.Service.ShapeService;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        ApplicationContext context = new FileSystemXmlApplicationContext("C:\\Users\\Rajesh\\git\\spring\\SpringAop\\src\\main\\java\\com\\rajesh\\SpringAop\\spring.xml");
        //get me the bean with the particular class. no need to cast
        ShapeService shapeService = context.getBean("ShapeService", ShapeService.class);
        shapeService.getCircle().setNamedReturn("Dummy Name");
        System.out.println(shapeService.getCircle().getName());
        System.out.println(shapeService.getTriangle().getName());
        
    }
}
