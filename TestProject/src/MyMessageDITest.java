
public class MyMessageDITest {

	public static void main(String[] args){
		String msg="Hi Rajesh";
		String email="rajesh@gmail.com";
		String phone= "8665";
		MessageServiceInjector injector =null;
		Consumer app = null;
		//send email
		injector = new EmailServiceInjector();
		app = injector.getConsumer();
		app.processMessages(msg, email);
		
		//send sms
		injector = new SMSServiceInjector();
		app = injector.getConsumer();
		app.processMessages(msg, phone);
	}
}
