package com.rajesh.SpringDemo;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.beans.factory.*;

/**
 * Hello world!
 *
 */
public class App 
{ 
    public static void main( String[] args )
    {
    	ApplicationContext context = new FileSystemXmlApplicationContext("D:\\ECLIPSE_WORKSPACE\\GitLabRepo\\spring\\SpringDemo\\src\\main\\java\\com\\rajesh\\SpringDemo\\spring.xml");
    	Triangle t = (Triangle)context.getBean("triangle-alias");
    	t.draw();
    	//System.out.println(t.getNewPoint().getX());
    }
}
